<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <h1>Position pour la semaine</h1>


    <?php
        $tableauPrenom = array("Florence",
                                "Sixteen",
                                "Syriane",
                                "Fatima",
                                "Hayette",
                                "Nathalie",
                                "Grace",
                                "Isabelle",
                                "Marion",
                                "Laurie",
                                "Esma",
                                "Esther",
                                "Viviane",
                                "Léa",
                                "Otilia",
                                "Maude"
        );

        shuffle($tableauPrenom);    //melange les elements du tableau a chaque actualisation

        echo "<h3>Table 1: </h3>";
        $table1 = array_slice($tableauPrenom, 0, 4); //fractionne le tableau de noms et ne prend que 4 valeurs
        foreach ($table1 as $tableValue1) {
            echo "$tableValue1 - ";
        }   //Les foreach prennent le tableau de départ puis met les valeurs dans un deuxieme

        echo "<h3>Table 2: </h3>";
        $table2 = array_slice($tableauPrenom, 4, 4); //fractionne le tableau de noms et ne prend que 4 valeurs
        foreach ($table2 as $tableValue2) {
            echo "$tableValue2 - ";
        }

        echo "<h3>Table 3: </h3>";
        $table3 = array_slice($tableauPrenom, 8, 4); //fractionne le tableau de noms et ne prend que 4 valeurs
        foreach ($table3 as $tableValue3) {
            echo "$tableValue3 - ";
        }
        
        echo "<h3>Table 4: </h3>";
        $table4 = array_slice($tableauPrenom, 12, 4); //fractionne le tableau de noms et ne prend que 4 valeurs
        foreach ($table4 as $tableValue4) {
            echo "$tableValue4 - ";
        }
        
        
    ?>
    
</body>
</html>